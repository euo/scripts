; ---         Ted u� to snad bere v�echno            ---
; --- kdy� n�co nebude br�t kontaktujte m� na jabbru ---
; ---      attanon@jabbim.cz nebo ICQ: 335770965     ---

; ------------------    ChangeLog     ------------------
; TODO:
;     - Oby� ��py, �ipky, flusac� �ipky,
;     - n�vody zlat� ne
; 2.5 - loot v�ech v�c�, n�jak� p�vodn� verze
; 2.6 - p�id�n loot pytl��k�.. buguje
; 2.7 - p�id�n loot ��p� podle typu ��p�..
;     - p�id�na mo�nost lootit jen pen�ze
;     - Fix: Vyp�n�n� autolootu p�i smrti
;     - Fix: Pytl��ky upraveny, snad u� naposledy.. Pytl��ky loot� jen to co chci
; 2.8
;     - Refaktoring ochrany proti p�du
;     - Fix: P�id�na ochrana k ��p�m a k pytl��k�m
;     - Refaktoring lootu ��p�, snad to bude fungovat
; 2.8.1
;     - P�id�ny gemy do lootu. Snad sem vychytal v�echny jejich typy.
; 2.8.2
;     - Experiment�ln� checkbox na loocen� postav, pros�m otestujte to
;     - P�id�n checkbox na trofeje
; 2.8.3
;     - Opraven� kuch�n�, vracen� rozst��han�ch k��� zp�t
;     - P�id�n� oby� �ipek, v�ech projektil�
;     - P�id�n� svitku z andor HB
;     - Opraven� oby� ��p�
; 2.8.4
;     - P�id�n� s�dla, obnov, many, nasycen�, neviditelnosti, hrotov�ho jedu a n�jak�ch hadr� (ur�it� k�p�) do lootov�n�
;     - P�id�n� kuch�n� kdy� je postava v morfu had�ho v�le�n�ka (je pot�eba p�ehodit d�ku do ruky a pak vr�tit zp�t)
;     - Z�kaz locen� v hidu a v invisu
;     - Sn�en� dosahu loocen� t�l na 1 pole
; 2.8.5
;     - Zru�en� #charType hl�ky o vypnut� kuch�n�
; 2.8.6
;     - P�id�n� hv�zdn�ho saf�ru, lektvar� many, obnov, hedvabna nit QLF,
;       soska kone SJR, temna nit UAG a v�emo�n�ch jin�ch v�c�
;     - zru�en� kontroly jestli si nebo nejsi v morfu. Pouze pokud jsi v hadovi tak kuch� d�kou..
; -----------------


set %verze 2.8.6
; --- Prom�nn� na loot
set %cont ZJF_CKF_IUD_KKF_HKF_AUD_ZJF_CUD
set %regy JUF_WZF_KUF_MZF_KZF_RZF_JZF_SZF_UZF_ZMW_ETH_QZF_DAG_FUF_GUF_NU_JKW_XZF_VZF_EUF_DUF_FZF_OZF_HUF_IUF_NZF_EAG_XMW_TZF_PZF
set %sipy RWF
set %sipky LNK
set %kuzeperi EEG_VLK_DDG_NWK_WLI_QRL
set %kuze EEG
set %rozKuze DDG
set %maso AAA_VRD_PUD_VLI_LLI_MRL_KRL
set %svitky LMF
set %lektvary NUF_ZUF_AVF_UUF_TUF_OUF_XUF_YUF_SUF_WUF_AQW_PUF_RCJ_VLF_IPF_JMF_CDJ_RJC_GPF
set %hulky CFF_HFF_BFF_IFF
set %penize POF_CPF
set %gemy CWF_BVF_UVF_VVF_RVF_MVF_HVF_FVF_GVF_OVF_XVH_VUF_BCF
set %hadry AMF_VDI_FCI_FZH_DWL_VQL_EDI_HCM_XVL_FZH_FCI_LDI_VDI_TQL_FWL_WQL_AWL_IZH_KHO_GCI_ZLF_HZH
set %kuchitems WLI
set %trophy MJW_OWI_WLI_YWK_IJW_RVH
set %cenosti QLF_SJR_UAG

set %penize %penize , _ , %gemy , _ , %cenosti ; p�id�ny gemy a cenosti do pen�z
; -----------------------
; --- Prom�nn� pro chod programu

set #menuButton 0
set %zadano 0
set %zaviranigumpu 0
set %loot 0
set %vybrano 0
set %sipyNaTyp 0
set %sipyTypName zadny

Finditem CNF_BNF C
if #FINDCNT > 0
{
  set %stahovaci_kudla #FindID
}
else  
{
  set %stahovaci_kudla 999
}

set %lastStahovaciKuda %stahovaci_kudla

Finditem TSF C
if #FINDCNT > 0
{
  set %itemDagger #FindID
}
else
{
  set %itemDagger 999
}

set %dyka 999

gosub autoloot_menu

cyklus:
gosub button

if %loot = 1
{
  GoSub MessageGreen Autoloot v , %verze spusten!
  Loop:
  {
    if #CHARGHOST = yes
    {
      ;pokud zemrete autoloot se automaticky vypne
      GoSub AutoLootVypni
      GoSub MessageRed AutoLoot vypnut!
      set #menubutton 0
      goto cyklus
    }
    
    if #chartype = AB ; jsem v hadovi, nastav�m d�ku kdy� ji m�m
    {
       set %stahovaci_kudla 999
       set %dyka %itemDagger
    }
    else
    {
       set %stahovaci_kudla %lastStahovaciKuda
       set %dyka 999
    }

    gosub button
    goto AutoLoot
  }

  AutoLoot:
  {
    if #charstatus = H
    {
      wait 5
      goto loop
    }
    
    Finditem YFM G_1 ;najde telo 2pole od vas
    set %idtelo #findid
    if #FINDDIST <= 2
    {
      if %kuch = 1
      {
        ;set #lobjectid %zbranid 
        ; pokud se behem inicializace nenasel stahovaci nuz, pouzivame aktualne drzenou zbran, jinak stahovaci nuz
        if %stahovaci_kudla = 999
        {
          if %dyka <> 999
          {
             set #lobjectid %dyka
             event Macro 17
             wait 5
          }
          else
          {
              event Macro 1 0 .usehand
          }
        }
        else
        {
          set #lobjectid %stahovaci_kudla        
          event Macro 17
        }		
        wait 5
        set #lTargetID %idtelo
        event Macro 22 0 ; lTarget
        wait 5
        
        if %dyka <> 999
        {
           event macro 24 2
           wait 5
        }
      }


      set #LOBJECTID %idtelo
      nextCPos 620 330 ; 858 420 souoadnice kde se otevoe gump mrtvoly
      event macro 17 ;otev�e mrtvolu
      wait 6
      set %idMrtvoly #LOBJECTID

      if %lootHrac = 0
      {
        for %i 15 1
        {
          scanJournal %i
          if ( ( prohledava in #journal ) && ( #charname in #journal ) )
          {
            event ExMsg %idMrtvoly 3 0 Hrace nelootim
            ignoreItem %idMrtvoly
            goto loop
          }
        }
      }

      event ExMsg %idMrtvoly 3 0 Lootim
      LootujMrtvolu:
      {
         GoSub OchranaProtiPadu %idMrtvoly
         if #result <> 0
            goto OchranaProtiPaduLoot

        findItem %lootItems C_ , %idMrtvoly ;najde item v mrtvole
        if #findKind = 0
        {
          GoSub LootniItem #findid #findstack ; vezme item a hod� ho do b�glu
          if #FINDCNT > 1
          {
            ;looti itemy (vsechny)
            goto LootujMrtvolu
          }
        }

        GoSub LootniItemySipy
        if #result <> 0 ; kv�li ochran� kv�li p�du u ��pu.. jinak by to ignorovalo
           ignoreItem %idMrtvoly ;vyloue� danou mrtvolu z dal��ho hled�n�


        if %zaviranigumpu = 1
          click 700 420 r; 924 540 r zavoe gump mrtvoly

        set #targcurs 0
        }
        if %strihkuze = 1
        {
          finditem %kuze C_ , %batoh
          if #findstack > 0
          {
             set #lobjectid #findid
             event macro 17
             wait 6
             finditem %rozKuze C_ , #BACKPACKID
             if #findstack > 0
             {
                GoSub LootniItem #findid #findstack
             }
          }
        }
        OchranaProtiPaduLoot:
      }
      goto loop
    }
  }
}
goto cyklus

Sub LootniItemySipy
if %sipyNaTyp <> 0
{
  LootujMrtvoluSipy:
  GoSub OchranaProtiPadu %idMrtvoly
  if #result <> 0
     Return 0

  findItem %sipyTypId C_ , %idMrtvoly
  if #findKind = 0
  {
    if %sipyNaTyp = 1 ; name ��p�
    {
      GoSub GetName #findid
      set %sipyTempName #result
      if %sipyTypName in %sipyTempName
      {
         GoSub LootniItem #findid #findstack
      }
      else
      {
         ignoreItem #findid
      }
    }

    if %sipyNaTyp = 2 ; color ��p�
    {
      if %sipyTypColor = #FINDCOL
      {
         GoSub LootniItem #findid #findstack
      }
      else
      {
         ignoreItem #findid
      }
    }
    
    if %sipyNaTyp = 3 ; ne�e�it, br�t
    {
      GoSub LootniItem #findid #findstack
    }
  }

  if #FINDCNT > 1
  {
   goto LootujMrtvoluSipy
  }
}
Return 1

Sub OchranaProtiPadu ; %1 ID itemu �i mrtvoly, Return 1 / 0 ( Je pot�eba / Nen� )
findItem %1 G_10
if #finddist > 2
   return 1

Return 0

Sub LootniItem ; %1 id %2 stack
  Exevent Drag %1 %2 ;sebere item
  wait 7
  Exevent Dropc %batoh ;d� item do batohu
  wait 7
Return

Sub AutoLootVypni
  menu delete zapvyp
  menu Button zapvyp 152 52 109 37 Zapnout autoloot
  set %zadano 0
  set %loot 0
  set %lootItems N/A
Return

sub button
    if #menuButton = zapvyp
    {
     if %loot = 0
     {
       set %loot 1
       menu delete zapvyp
       menu Button zapvyp 152 52 109 37 Vypnout autoloot
       gosub zjisteniid
       set #menuButton 0
     }
     else
     {
       GoSub AutoLootVypni
       set #menubutton 0
       GoSub MessageGreen Autoloot vypnut.
       goto cyklus
     }
    }
    if #menuButton = newbagl
    {
      menu hide autoloot_menu
      gosub TargetBagl

      set #menubutton 0
      menu show autoloot_menu
      goto cyklus
    }
    if #menuButton = lootbagl
    {
      gosub VyberBagl
      set #menubutton 0
    }
    if #menuButton = CLOSED
    {
     halt
    }
return

sub VyberBagl
  if %loot <> 1
  {
     GoSub MessageRed Mus� m�t zapnut� loot.
     return 1
  }
  set #ltargetid 0
  GoSub MessageTarget Vyber batoh na vybrani!
  set #targCurs 1
  cilVyberBatoh:
  if #targCurs = 1
  goto cilVyberBatoh
  set %batohVyber #lTargetID

  set #lobjectid %batohVyber
  event macro 17 0
  wait 7

  lootojPytlik:
    GoSub OchranaProtiPadu %batohVyber
    if #result <> 0
        goto OchranaProtiPaduPytlik

    findItem %lootItems C_ , %batohVyber
    if #findKind = 0
    {
      GoSub LootniItem #findid #findstack ; vezme item a hod� ho do b�glu
      if #FINDCNT > 0
      {
        ;looti itemy (vsechny)
        goto lootojPytlik
      }
    }
    GoSub LootniItemySipy ; A� vybere ��py, kdy� budu cht�t jen n�jak�

  OchranaProtiPaduPytlik:
  set %batohVyber 0
Return 1

sub zjisteniid
 if %zadano = 0
 {
   menu get vsechno
   if #menures
   {
    set %sipyNaTyp 0 ; projistotu znovu vypnout, aby to neblbo
    set %lootItems %regy , _ , %sipy , _ , %sipky , _ , %kuzeperi , _ , %maso , _ , %lektvary , _ , %svitky , _ , %hulky , _ , %penize , _ , %hadry
   }
   else
   {
    gosub check regy
    gosub CheckSipy
    gosub check kuzeperi
    gosub check maso
    gosub check lektvary
    gosub check svitky
    gosub check hulky
    gosub check penize
    gosub check hadry
    gosub check trophy
   }
   if %lootItems = N/A
   {
      menu hide autoloot_menu
      display OK Musis neco vybrat
       if #dispres = ok
       {
          menu show autoloot_menu
          set #menubutton 0
          set %loot 0
          menu delete zapvyp
          menu Button zapvyp 152 52 109 37 Zapnout autoloot
          goto cyklus
       }
   }
  gosub nastaveni
  set %zadano 1
 }
; Debug: event sysmessage %lootItems
return

Sub CheckSipy
    menu get sipytyp
    if #menures = 1 ; Oby� ��py
    {
       set %sipyTypColor 0
       set %sipyNaTyp 2
       set %sipyTypId %sipy
    }
    if #menures = 2
    {
       ; Elf�
       set %sipyNaTyp 1
       set %sipyTypName elf
       set %sipyTypId %sipy
    }
    if #menures = 3
    {
       ; Smrt�c�
       set %sipyNaTyp 1
       set %sipyTypName Smrt
       set %sipyTypId %sipy
    }
    if #menures = 4
    {
       ; v�echny ��py
       set %sipyNaTyp 3
       set %sipyTypId %sipy
    }
    if #menures = 5
    {
       ; oby� �ipky
       set %sipyTypColor 0
       set %sipyNaTyp 2
       set %sipyTypId %sipky
    }
    if #menures = 6
    {
       ; v�echny projektily
       set %sipyNaTyp 3
       set %sipyTypId %sipy , _ , %sipky
    }
Return


sub nastaveni
  ; === Kuchani on/off ===
  set %kuch 0
  menu Get kuchani
  if #menures
  {
   GoSub MessageGreen Kuch�n� t�l zapnuto.
   set %kuch 1
  }
    ; === Strihani on/off ===
  set %strihkuze 0
  menu Get strihani
  if #menures
  {
   finditem KAG C_
   if #findid <> X
   {
     GoSub MessageGreen St��h�n� k��� zapnuto.
     set %strihkuze 1
   }
   else
     GoSub MessageRed Mus� m�t u sebe n��ky, jinak ti to st��hat nebude.
  }

    ; === LootHr��� on/off ===
    set %lootHrac 0
    menu Get lootHrac
    if #menures
    {
       GoSub MessageGreen Loot hr��� zapnut.
       set %lootHrac 1
    }

    ; === Zav�r�n� gump� on/off ===
  set %zaviranigumpu 0
  menu Get zaviranigump
  if #menures
  {
   GoSub MessageGreen Zav�r�n� gumpu t�la zapnuto.
   set %zaviranigumpu 1
  }
  
  menu show autoloot_menu
  if %vybrano <> 1 ; pokud u� vybere pomoc� tla��tka, nen� pot�eba znovu p�i zapnut�
    gosub TargetBagl

return

Sub TargetBagl
  ; === Vyber batuzek do ktereho budes sbirat ===
  znovuvyberbagl:
  set #ltargetid 0
  GoSub MessageTarget Vyber batoh na sbirani!
  set #targCurs 1
  cil32:
  if #targCurs = 1
  goto cil32
  set %batoh #lTargetID

  if #ltargetid = YC  ; ochrana kv�li pou�it� ESC
  {
    set %vybrano 0
    return 2
  }

  finditem #ltargetId
  if ( #findid <> X ) && ( #findbagid <> #backpackid )
  {
     finditem #findbagid C_ , #backpackid
     if #findid <> X
        finditem %batoh
  }

  GoSub GetRestProperty %batoh ; kontrola jestli je to ba�oh
  if ( Predmetu in #result ) || ( %batoh = #backpackid )
  {
    GoSub GetName %batoh
    GoSub MessageGreen Vybral si ba�oh s n�zvem #result
  }
  else
  {
    GoSub MessageRed Mus� vybrat ba�oh, ne jinou v�c. V p��pad� n�jak� chyby kontaktuj Skaze.
    goto znovuvyberbagl
  }
  set %vybrano 1
return 1

; ==== Menu ====
sub autoloot_menu
  GoSub CheckVersion
  set %checkVerze #result
  
	menu Clear
	menu Window Title Autoloot v , %verze
	menu Window Color BtnFace
	if %checkVerze = ok
  {
    menu Window Size 300 350
  } 
  else
  {
    menu Window Size 300 380
  } 
	menu Font Name MS Sans Serif
	menu Font Size 8
	menu Text byAtt 210 330 (c) Attanon (Skaz)
	menu Font Transparent #true
	menu Font Align Right
	menu Font Name MS Sans Serif
	menu Font Size 12
	menu Font Style b
	menu Font Color WindowText
	menu Font Align Left
	menu Text EUOLabel1 16 16 Vyber v�ci kter� chce�  l��tit
	menu Font Size 8
	menu Font Style
	; ----- Itemy kter� m��e lootit
	menu Check regy 16 44 117 25 #true Regy
	GoSub AddComboBoxSipy
	menu Check kuzeperi 16 100 117 25 #false K��e a pe��
	menu Check maso 16 128 117 25 #false Maso
	menu Check lektvary 16 156 117 25 #true Lektvary
	menu Check svitky 16 184 117 25 #true Svitky
	menu Check hulky 16 212 117 25 #false H�lky
	menu Check penize 16 240 117 25 #true Pen�ze a kam�nky
	menu Check hadry 16 268 117 25 #true Oble�en�
	menu Check trophy 160 184 117 25 #false Trofeje
	menu Font Style b
	menu Check vsechno 16 296 117 25 #false V�echno
	menu Font Style
	; ----- Tla��tka
	menu Button zapvyp 152 52 109 37 Zapnout autoloot
	menu Button newbagl 152 90 109 37 Ur�it batoh
	menu Button lootbagl 152 128 109 37 Vybrat Pytlik / Truhlu
  ; ----- Checkboxy zbytek
  menu Check lootHrac 160 212 117 25 #true Loot hr���
	menu Check kuchani 160 240 117 25 #false Kuch�n� t�l
	menu Check strihani 160 268 117 25 #false St��h�n� k���
	menu Check zaviranigump 160 296 117 25 #true Zav�r�n� gumpu t�la

  if %checkVerze = ok
  {
    menu text 1 20 330 M� aktu�ln� verzi
  }
  if %checkVerze = new
  {
    menu font color red
    menu text 1 20 330 Nem� aktu�ln� verzi.
    menu text 1 20 350 Stahuj na https://euo.gitlab.io/scripts
  }
     
	menu Show 421 270
return

sub CheckVersion
  send HTTPPost euo.gitlab.io /scripts/checks/autoloot_verze.euo

  if %verze = %nova
  {
    Return ok
  }
  
  if %verze <> %nova
  {
    Return new
  }
Return

sub AddComboBoxSipy
  menu combo create sipytyp 16 72 117
  menu combo add sipytyp Oby� ��py
  menu combo add sipytyp Elf� ��py
  menu combo add sipytyp Smrt�c� ��py
  menu combo add sipytyp V�echny ��py
  menu combo add sipytyp Oby� �ipky
  menu combo add sipytyp V�echny projektily
  menu combo add sipytyp ��dn� ��py
  menu combo select sipytyp 2
Return

sub check ; {polo�ka lootovaci}
  menu get %1
  if #menures
  {
    if %lootItems = N/A
    {
      set %lootItems % . %1
    }
    else
    {
      set %lootItems %lootItems , _ , % . %1
    }
  }
return

Sub GetName       ;Jeleni$Druh: Jelen$
  event Property %1
  set %prop #property
  str pos %prop $
  set %pos #strRes - 1
  str left %prop %pos
Return #strres

Sub MessageRed
  set #sysmsgcol 38
  set %msgred
  for %_cnt 1 %0
  {
    set %msgred %msgred , % . %_cnt , #spc
  }
  event sysmessage %msgred
  set #sysmsgCol 0
Return

Sub MessageGreen
  set #sysmsgcol 68
  set %msggreen
  for %_cnt 1 %0
  {
    set %msggreen %msggreen , % . %_cnt , #spc
  }
  event sysmessage %msggreen
  set #sysmsgCol 0
Return

Sub MessageTarget
  set #sysmsgcol 1668
  set %msgtarg
  for %_cnt 1 %0
  {
    set %msgtarg %msgtarg , % . %_cnt , #spc
  }
  event sysmessage Target: , #spc , %msgtarg
  set #sysmsgCol 0
Return

Sub MessageOrange
  set #sysmsgcol 1925
  set %msggreen
  for %_cnt 1 %0
  {
    set %msggreen %msggreen , % . %_cnt , #spc
  }
  event sysmessage %msggreen
  set #sysmsgCol 0
Return

Sub GetRestProperty
  event Property %1
  set %prop #property
  str len %prop
  set %delka #strRes
  str pos %prop $
  set %pos #strRes
  set %vzit %delka - %pos
  str right %prop %vzit
return #strRes
