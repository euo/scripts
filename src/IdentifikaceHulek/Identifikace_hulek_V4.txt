;Script ktery vezme hulku z preddefinovan�ho uloziste (mu�e byt i pytel ve skrini),
;hulku oznaci a opet ji ulozi do definovan�ho vaku, nebo bedynky

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;Writed by Walldezz 1_2011;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

set %Hulky BFF_HFF_IFF_CFF

;nastaveni uloziste s NEidentifikovanymi hulkami (odkud brat)
event sysmessage Vyber batoh s hulkami k oznacen�.
set #TargCurs 1
tgloop:
if #TargCurs = 1
goto tgloop
set %Pytel_neidentifikovaneID #lTargetID
wait 1s

;nastaveni uloziste s identifikovanymi hulkami (kam davat)
event sysmessage Vyber batoh kam se maji oznacene hulky ulozit.
set #TargCurs 1
tglooop:
if #TargCurs = 1
goto tglooop
set %Pytel_identifikovaneID #lTargetID
gosub Hlavni_cast

;otevre batoh s hulkami a batoh na hulky
sub Otevri_uloziste
set #lobjectid %Pytel_neidentifikovaneID
event macro 17
wait 10
set #lobjectid %Pytel_identifikovaneID
event macro 17
wait 10
return

;uklidi identifikovanou hulku do pytle
sub Uklid_hulku
 finditem %Aktualni_hulka
 exevent drag #FindID #FindStack
 wait 10
 exevent dropc %Pytel_identifikovaneID
 wait 10
return

;vybere neidentifikovanou hulku z pytle
sub Vyber_hulku
 finditem %Hulky C_ , %Pytel_neidentifikovaneID
 if #findcnt >= 1
   {
     exevent drag #FindID 1
     wait 10
     exevent dropc #BackPackID
     wait 10
     set %Aktualni_hulka #FindID
   }
 else #findcnt = 0
   {
     event sysmessage Batoh je prazdny, neni co urcovat ukoncuji script.
     goto VEN
   }
 wait 10
return

;identifikuje hulku
sub Urci_hulku
  set #lTargetID %Aktualni_hulka
  event macro 1 0 .startability 14
  wait 0,5s
  event macro 22
  wait 10s
return

Sub Hlavni_cast
  gosub Otevri_uloziste
  dokola:
    gosub Vyber_hulku
    gosub Urci_hulku
    gosub Uklid_hulku
  goto dokola
  ven:
halt
