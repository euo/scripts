set %msg_hid HID , #spc , ZA:
set %pauza 5
set %hlaska1 1!
set %hlaska2 2!

set %dvere PJC_YOC_NJC_QJC_HXD_UBD_PJG

set #lpc 1000
repeat
; ==== Hotkeye od t�hle ��dky m��ete vkl�dat nov� ========
  onHotkey Y | CTRL
           gosub hid
  onHotkey Z | CTRL
           gosub hid
  onHotkey A | CTRL
           gosub NastavProvokace
  onHotkey S | CTRL
           gosub Provokuj
  onHotkey G | CTRL
           gosub FindTarget
  onHotkey D | CTRL
           gosub OtocProvok
  onHotkey F | CTRL
           gosub UkazTarget
  onHotkey T | CTRL
           gosub OtevriDvere
  onHotkey A | CTRL | ALT
           gosub Ochoc
; ==== A� do t�hle ��dky m��ete vkl�dat nov� ====
  sleep 1
until #false

sub targetOne
   event sysmessage
   set #targcurs 1
   targetOneLoop:
   if #targcurs = 1
      goto targetOneLoop
return


; ========= Tuhle ��st skriptu neupravuj pokud nev� co d�l� ==============

Sub Ochoc
    event sysmessage Vyber koho by si r�d ocho�il.
    set #targcurs 1
     tgloopOchoc:
     if #targcurs = 1
     goto tgloopOchoc
     set %ochoc #LTARGETID
     
     GoSub GetName %ochoc
     event sysmessage Ocho�uji: #result
     
     tamuj:
     event macro 13 35
     wait %pauza
      set #ltargetid %ochoc
      event macro 22
      
     set %jrnl #jindex
      while #true
         {
         if #jindex > %jrnl
            {
            set %jrnl %jrnl + 1
            scanjournal %jrnl
            if nedok�zal in #journal
               {
                         goto tamuj
               }
            }
            
             if p�ij�m� in #journal || nevid� in #journal
               {
                         goto ochocKonec
               }
            
            wait 20
         }
      ochocKonec:
     
return

Sub hid
   gosub odpocet 3 %msg_hid
Return

Sub FindTarget
  ignoreitem reset
  set %finded 1
  findingTarget:
  finditem IS G_10
  if #findKind = 1
  {
    ignoreitem #findid
    if %finded = 1
      set %os1 #findid

    if %finded = 2
      set %os2 #findid

    set %finded %finded + 1
    if %finded >= 3
    {
      goto findingTargetEnd
    }
    wait 20
    goto findingTarget
  }
  findingTargetEnd:

  GoSub GetName %os1
  set %jmeno1 #result
  GoSub GetName %os2
  set %jmeno2 #result
  gosub UkazTarget
Return


sub odpocet
  for %i %1 1
  {
      set %msg_odpocet %2 , #spc , %i
      event exMsg #charid 3 54 %msg_odpocet
      wait 1s
  }
return

Sub OtevriDvere
  GoSub findClosest %dvere G_2
  if #result <> -1
  {
    event exmsg #result 3 20 Tyhle beru
    set #lobjectid #result
    event macro 17
    wait 10
  } else {
    event sysmessage Dvere nenalezeny
    wait 10
  }
Return

Sub useTarget
   set #LTARGETID
   set #targcurs 1
   useTargetloop:
   if #targcurs = 1
      goto useTargetloop
Return #LTARGETID

sub NastavProvokace
  set #sysmsgcol 54
  event sysmessage Vyber prvn�ho, kter�ho by jsi r�d provokoval.
  GoSub useTarget
  set %os1 #result

  event sysmessage Vyber druh�ho, kter�ho by jsi r�d provokoval.
  tgloop2:
  GoSub useTarget
  if #result = %os1
  {
    event sysmessage Vyber druh�ho, kter�ho by jsi r�d provokoval. A nevyb�rej stejn�ho.
    set #targcurs 1
    goto tgloop2
  }
  set %os2 #LTARGETID
  set #sysmsgcol 0
  GoSub GetName %os1
  set %jmeno1 #result
  GoSub GetName %os2
  set %jmeno2 #result
return

Sub UseSkillProvocation
  event macro 1 0 .provokuj
  wait %pauza
  set #ltargetid %1
  set #ltargetkind 1
  event macro 22
  wait %pauza
  set #ltargetid %2
  set #ltargetkind 1
  event macro 22
return

Sub findClosest
    NAMESPACE push
    NAMESPACE local CavsFindNearestItem
    
    set !lastClosest -1
    set !lastClosestDistance 999
    findClosestPoint:
    Finditem %1 %2
    if #findcnt > 0
    {
       ignoreitem #findid 999

       if #finddist <= !lastClosestDistance
       {
          set !lastClosestDistance #finddist
          set !lastClosest #findid
       }
       goto findClosestPoint
    }
    
    ignoreitem reset 999
    set #result !lastClosest
    NAMESPACE clear
    NAMESPACE pop
Return #result

Sub Provokuj
  GoSub UseSkillProvocation %os1 %os2
  GoSub Message 1 1
  wait %pauza
  GoSub Message 2 2
return

Sub OtocProvok
  GoSub UseSkillProvocation %os2 %os1
  GoSub Message 2 1
  wait %pauza
  GoSub Message 1 2
return

Sub UkazTarget
  GoSub Message 1 1
  wait 2
  GoSub Message 2 2
return

Sub GetName       ;Jeleni$Druh: Jelen$
  event Property %1
  set %prop #property
  str pos %prop $
  set %pos #strRes - 1
  str left %prop %pos
Return #strres

Sub Message
  ;%1 = jestli prvn� nebo druhej %2 = hl�ka
  set %zobraz %jmeno . %1
  set %zobrazpc %hlaska . %2
  set %zobrazid %os . %1
  Event ExMsg %zobrazid 3 74 %zobraz , : , #spc , %zobrazpc
return
