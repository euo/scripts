publicDir="$(pwd)/../public"
buildDir="$publicDir/build"
publiChecksDir="$publicDir/checks"
checksDir="$(pwd)/../checks"
srcDir="$(pwd)/../src"
mkdir -p "$buildDir"

rm -Rf "$buildDir/*"

cat build-header.html > "$publicDir/index.html"

for D in `find ../src/* -type d -not -path '*/\.*' -not -name '.*'`
do
    path="$srcDir${D:6:1000}"
    zipName="$(basename "$path").zip"
    
    zip -rjq "$buildDir/$zipName" "$path"
    echo -e "Building: \n\tFile: \t\t$zipName"
    description=""
    if [ -f "$path/description.txt" ]; then
        description=`cat $path/description.txt`
        echo -e "\tDescription: \tparsing description.txt"
    else 
        echo -e "\tDescription: \tnot exists"
    fi
    echo "<tr><td><a href=\"build/$zipName\">$zipName</a></td><td>$description</td></tr>" >> "$publicDir/index.html"
    echo -e "\n"
done

cat build-footer.html >> "$publicDir/index.html"

mkdir -p "$publiChecksDir"
cp $checksDir/*.euo "$publiChecksDir/"
echo "Copy checks into checks folder. You can find it under https://euo.gitlab.io/scripts/checks/"