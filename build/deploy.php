<?php

require_once __DIR__ . '/vendor.phar';

Tracy\Debugger::enable(\Tracy\Debugger::DEVELOPMENT, __DIR__ . '/log');

$finder = new \Symfony\Component\Finder\Finder();

$finder->directories()->in(__DIR__ . '/../src');

$buildDir = __DIR__ . '/../public/build/';

\Tracy\Debugger::timer('all');

@mkdir($buildDir, 0777, TRUE);

$output = new Symfony\Component\Console\Output\ConsoleOutput();

$scripts = [];
foreach ($finder as $script) {
	$scriptName = $script->getFilename();
	$files = new \Symfony\Component\Finder\Finder();
	$files->in($script->getRealPath())->files()->ignoreDotFiles(TRUE)->notName('description.txt');

	$zip = new ZipArchive();
	$zip->open($buildDir . $scriptName . '.zip', ZipArchive::CREATE);
	foreach ($files as $file) {
		$zip->addFile($file->getRealPath(), $file->getFilename());
	}

	$output->write("Building <comment>$scriptName</comment> into zip.");

	$description = '';

	if (file_exists($script->getRealPath() . '/description.txt')) {
		$output->write(' Parsing <comment>description.txt</comment>');
		$description = file_get_contents($script->getRealPath() . '/description.txt');
	}

	$output->writeln('');

	$scripts[] = [
		'name'        => $scriptName,
		'path'        => 'build/' . $scriptName . '.zip',
		'description' => $description,
	];

	$zip->close();
}

$latte = new Latte\Engine();
$file = $latte->renderToString(__DIR__ . '/build-page.latte', [
	'scripts' => $scripts,
]);

file_put_contents(__DIR__ . '/../public/index.html', $file);

$output->writeln(sprintf('<info>Scripts was builded in %0.2f seconds.</info>', \Tracy\Debugger::timer('all')));

$finderChecks = new \Symfony\Component\Finder\Finder();
$finderChecks->files()->in(__DIR__ . '/../checks');

$checksDir = __DIR__ . '/../public/checks/';

\Tracy\Debugger::timer('checks');

@mkdir($checksDir, 0777, TRUE);

foreach ($finderChecks as $check) {
	copy($check->getRealPath(), $checksDir . '/' . $check->getFilename());
	$output->writeln(sprintf('Check <comment>%s</comment> was deployed', $check->getFilename()));
}